<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mengajar extends Model
{
    use HasFactory;

    protected $table = 'mengajar';
    protected $fillable = ['id_mapel', 'id_mapel', 'nip'];

    public function guru(){
        return $this->belongsTo(Guru::class, 'nip');
    }

    public function kelas(){
        return $this->belongsTo(Kelas::class);
    }

    public function mapel(){
        return $this->belongsTo(Mapel::class);
    }
}
