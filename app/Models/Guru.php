<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Database\Eloquent\Casts\Attribute;

class guru extends Authenticatable
{   
     use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $table = 'guru';
    protected $primaryKey = 'nip';

    protected $fillable = ['nip', 'username','level', 'password', 'nama', 'gender', 'alamat'];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected function role(): Attribute
    {
        return new Attribute(
            get: fn ($value) =>  ["kurikulum", "siswa", "walikelas", "guru"]
        );
    }

    public function kelas(){
        return $this->hasManyThrough(Kelas::class, Mengajar::class, 'nip', 'id', 'nip', 'id_kelas');
    }
    public function mapel(){
        return $this->hasManyThrough(Mapel::class, Mengajar::class, 'nip', 'id', 'nip', 'id_mapel');
    }
    public function mengajar(){
        return $this->hasMany(Mengajar::class, 'nip');
    }
}
