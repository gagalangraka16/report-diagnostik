<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KomponenModel extends Model
{
    use HasFactory;
    protected $table="komponen";
    public $timestamps = false;
}
