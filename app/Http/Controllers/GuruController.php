<?php

namespace App\Http\Controllers;

use App\Models\guru;
use App\Models\Kelas;
use App\Models\Mapel;
use App\Models\Siswa;
use App\Models\komponen;
use App\Models\Mengajar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class GuruController extends Controller
{
    public function dashboard()
    {
        $komponen = komponen::count();
        $mapel = mapel::count();
        return view('guru.dashboard', [
            'title' => 'Dashboard'
        ], compact(['mapel', 'komponen']));
    }

    // Komponen
    public function komponen()
    {
        $guru = Guru::where('nip', auth()->user()->nip)->with('kelas','mapel')->first();
        $kelas = Kelas::all();
        $mengajar = Mengajar::all();
        $mapel = Mapel::all();
        $komponen = komponen::all();
        return view('guru.komponen', [
            'title' => 'Komponen'
        ], compact('guru', 'kelas', 'mapel', 'mengajar', 'komponen'));
    }
    public function proses(Request $request){

            foreach($request->nama as $key => $value)
                {
           DB::table('komponen')->insert([
            'id'  =>  $request->id,
            'nama' => $value
        ]);
                }
            return redirect("/guru/komponen");    
        }    


     public function tambah()
    {
        return view('guru.tambah', [
            'title' => 'Tambah'
        ]);
    }
}
