<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\KomponenModel;

class KomponenController extends Controller
{
    //
    public function form()
        {
            return view('dinamis/form');
        }
    public function proses(Request $request)
        {
            foreach($request->nama_bahasa as $key => $value)
                {
                    $bahasa = new KomponenModel;    
                    $bahasa->nama=$value;
                    $bahasa->save();
                }
            return redirect("/guru");    
        }    
}
