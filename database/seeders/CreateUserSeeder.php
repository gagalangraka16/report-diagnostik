<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Guru;

class CreateUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $guru = [
            [
                'nip' => '32134321',
                'username' =>  'Admin',
                'level' =>  'kurikulum',
                'password'  =>  bcrypt('12345'),
                'nama' => 'admin',
                'gender' => 'L',
                'alamat' => 'JL',
                'tgl_lahir' => '2022-11-07'
            ],
            [
                'nip' => '321334253',
                'username' =>  'Admin1',
                'level' =>  'walikelas',
                'password'  =>  bcrypt('12345'),
                'nama' => 'admin1',
                'gender' => 'L',
                'alamat' => 'JL',
                'tgl_lahir' => '2022-11-07'
            ],
            [
                'nip' => '32133223',
                'username' =>  'Admin2',
                'level' =>  'guru',
                'password'  =>  bcrypt('12345'),
                'nama' => 'admin2',
                'gender' => 'L',
                'alamat' => 'JL',
                'tgl_lahir' => '2022-11-07'
            ]
        ];

        foreach ($guru as $user) {
            Guru::create($user);
        }
    }
}
