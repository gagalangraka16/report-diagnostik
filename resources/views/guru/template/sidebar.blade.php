 <ul class="navbar-nav sidebar sidebar-dark accordion toggled" id="accordionSidebar" style="background-color:#20201D;">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="/kurikulum">
                <div class="sidebar-brand-icon ">
                    <img src="{{asset('template/img/kemdikbud.png')}}" style="height: 50px;">
                </div>
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item {{ Request::is('guru') ? 'active' : '' }}">
                <a class="nav-link" href="/guru">
                    <i class="fas fa-fw fa-home"></i>
                    <span>Dashboard</span></a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider">

            <!-- Nav Item - Pages Collapse Menu -->
            <li class="nav-item {{ Request::is('guru/komponen*') ? 'active' : '' }}">
                <a class="nav-link" href="/guru/komponen">
                    <i class="fas fa-fw fa-id-card"></i>
                    <span>Komponen</span>
                </a>
            </li>

            <!-- Nav Item - Utilities Collapse Menu -->
            <li class="nav-item {{ Request::is('guru/data_guru*') ? 'active' : '' }}">
                <a class="nav-link" href="/guru/data_guru">
                    <i class="fas fa-fw fa-id-card"></i>
                    <span>Data Guru</span>
                </a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider">

            <!-- Nav Item - Pages Collapse Menu -->
            <li class="nav-item {{ Request::is('kurikulum/data_mapel*') ? 'active' : '' }}">
                <a class="nav-link" href="/kurikulum/data_mapel">
                    <i class="fas fa-fw fa-folder"></i>
                    <span>Data Matapelajaran</span>
                </a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider d-none d-md-block">
        </ul>